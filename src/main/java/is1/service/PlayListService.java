package is1.service;

import is1.model.PlayList;
import is1.repository.PlayListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;

@Service
public class PlayListService extends FollowableService<PlayList> {

    @Autowired
    private PlayListRepository playListRepository;

    @Autowired
    private AccountService accountService;

    public PlayList createPlayList(PlayList playList) {
        if (playList.getCreatedOn() == null)
            playList.setCreatedOn(new Timestamp((new java.util.Date()).getTime()));
        if (playList.getFeed() == null)
            addFeed(playList);
        accountService.addFeedEntry(playList.getOwner(), "Created playlist \"" + playList.getTitle() + "\"");
        playListRepository.save(playList);
        setFeedUrl(playList);
        return playList;
    }

    public PlayList getPlayList(long id) {
        return playListRepository.findById(id);
    }

    public List<PlayList> getPlayLists(int firstResult, int maxResults) {
        return playListRepository.findEntries(firstResult, maxResults);
    }

    public PlayList updatePlayList(PlayList playList) {
        return playListRepository.merge(playList);
    }

}
