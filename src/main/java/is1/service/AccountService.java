package is1.service;

import is1.model.Account;
import is1.model.FeedEntry;
import is1.model.Followable;
import is1.repository.AccountRepository;
import is1.repository.FeedEntryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountService extends FollowableService<Account> {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private FeedEntryRepository feedEntryRepository;

    public Account createAccount(String username, String email, String password, String role) {
        Account account = new Account(username, email, password, role);
        addFeed(account);
        accountRepository.save(account);
        return account;
    }

    public Account createAccount(Account account) {
        if (account.getFeed() == null)
            addFeed(account);
        accountRepository.save(account);
        setFeedUrl(account);
        return account;
    }

    public void updateAccount(Account account) {
        accountRepository.merge(account);
    }

    public Account getAccount(long id) {
        return accountRepository.findById(id);
    }

    public Account getAccountByUsername(String username) {
        return accountRepository.findByUsername(username);
    }

    public List<FeedEntry> getNotifications(Account account, int firstResult, int maxResults) {
        return feedEntryRepository.getLatestEntriesForAccount(account,
                firstResult, maxResults);
    }

    public void addFollowing(Account account, Followable followable) {
        account.addFollowing(followable.getFeed());
        accountRepository.merge(account);
    }
}
