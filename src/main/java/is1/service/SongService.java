package is1.service;

import is1.model.Account;
import is1.model.Song;
import is1.model.Like;
import is1.repository.LikeRepository;
import is1.repository.SongRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SongService{

    @Autowired
    private SongRepository songRepository;

    @Autowired
    private LikeRepository likeRepository;

    public Song createSong(Song song) {
        songRepository.merge(song);
        return song;
    }

    public List<Song> getTopSongs(int firstResult, int maxResults) {
        return songRepository.getTop(firstResult, maxResults);
    }

    public List<Song> searchSongs( String query ) {
        return songRepository.getSearchResults(query);
    }

    public Song getSong(long id) {
        return songRepository.findById(id);
    }

    public void likeSong(Account account, Song song) {
        if (!likeRepository.likes(account, song))
            likeRepository.merge(new Like(account, song));
    }

}
