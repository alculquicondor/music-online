package is1.service;

import is1.model.Author;
import is1.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorService extends FollowableService<Author> {

    @Autowired
    private AuthorRepository authorRepository;

    public Author createAuthor(String name, String bio) {
        Author author = new Author(name, bio);
        addFeed(author);
        authorRepository.save(author);
        return author;
    }

    public Author createAuthor(Author author) {
        if (author.getFeed() == null)
            addFeed(author);
        authorRepository.save(author);
        setFeedUrl(author);
        return author;
    }

    public Author getAuthor(long id) {
        return authorRepository.findById(id);
    }

    public List<Author> getAuthors(int firstResult, int maxResults) {
        return authorRepository.findEntries(firstResult, maxResults);
    }

}
