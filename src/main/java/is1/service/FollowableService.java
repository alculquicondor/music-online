package is1.service;

import is1.model.Feed;
import is1.model.FeedEntry;
import is1.model.Followable;
import is1.repository.FeedEntryRepository;
import is1.repository.FeedRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
public abstract class FollowableService<F extends Followable> {
    
    @Autowired
    private FeedRepository feedRepository;

    @Autowired
    private FeedEntryRepository feedEntryRepository;

    protected void addFeed(F followable) {
        Feed feed = new Feed();
        feedRepository.save(feed);
        followable.setFeed(feed);
    }

    protected void setFeedUrl(F followable) {
        followable.getFeed().setUrl(followable.getLink());
        feedRepository.merge(followable.getFeed());
    }

    public FeedEntry addFeedEntry(F followable, String content) {
        FeedEntry feedEntry = new FeedEntry(followable.getFeed(),
                followable.getType() + " " + followable.getName() + ": " + content,
                new Timestamp((new java.util.Date()).getTime()));
        feedEntryRepository.save(feedEntry);
        return feedEntry;
    }

}
