package is1.bean;

import is1.model.Author;
import org.hibernate.validator.constraints.NotBlank;

public class AuthorBean {

    @NotBlank
    private String name;

    private String bio;

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Author createAuthor() {
        return new Author(name, bio);
    }
}
