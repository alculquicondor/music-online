package is1.bean;


import is1.model.Album;
import is1.model.Author;
import is1.service.AuthorService;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class AlbumBean {

    @NotBlank
    private String title;

    private long author;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getAuthor() {
        return author;
    }

    public void setAuthor(long author) {
        this.author = author;
    }

    public Album createAlbum() {
        List<Author> authors = new LinkedList<Author>();
        return new Album( title , new java.sql.Date(new Date().getTime()) , authors);
    }

}