package is1.bean;

import is1.bean.constraints.ScriptAssertField;
import org.hibernate.validator.constraints.*;

import is1.model.Account;

@ScriptAssertField(lang = "javascript", script = "_this.passwordVerify.equals(_this.password)",
        fieldName = "passwordVerify", message = "{password.mismatch.message}")
public class SignupBean {

	private static final String NOT_BLANK_MESSAGE = "{notBlank.message}";
	private static final String EMAIL_MESSAGE = "{email.message}";

    @NotBlank(message = SignupBean.NOT_BLANK_MESSAGE)
    private String username;

	@NotBlank(message = SignupBean.NOT_BLANK_MESSAGE)
	@Email(message = SignupBean.EMAIL_MESSAGE)
	private String email;

	@NotBlank(message = SignupBean.NOT_BLANK_MESSAGE)
	private String password;

    @NotBlank(message = SignupBean.NOT_BLANK_MESSAGE)
    private String passwordVerify;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

    public String getPasswordVerify() {
        return passwordVerify;
    }

    public void setPasswordVerify(String passwordVerify) {
        this.passwordVerify = passwordVerify;
    }

    public Account createAccount() {
		return new Account(getUsername(), getEmail(), getPassword(), "ROLE_USER");
	}
}
