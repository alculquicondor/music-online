package is1.bean;

import is1.model.Account;
import is1.model.PlayList;
import is1.model.Song;
import org.hibernate.validator.constraints.NotBlank;

import java.util.LinkedList;

public class PlayListBean {

    @NotBlank
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public PlayList createPlayList(Account account) {
        return new PlayList(title, new LinkedList<Song>(), account);
    }
}
