package is1.repository;

import is1.model.Account;
import is1.model.FeedEntry;

import java.util.List;

public interface FeedEntryRepository extends BaseRepository<FeedEntry, Long> {
    List<FeedEntry> getLatestEntriesForAccount(Account account, int firstResult, int maxResults);
}
