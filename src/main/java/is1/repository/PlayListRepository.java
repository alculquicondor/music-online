package is1.repository;

import is1.model.PlayList;

public interface PlayListRepository extends BaseRepository<PlayList, Long> {
}
