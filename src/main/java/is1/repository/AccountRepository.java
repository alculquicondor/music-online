package is1.repository;

import is1.model.Account;

public interface AccountRepository extends BaseRepository<Account, Long> {
	public Account findByUsername(String username);
}
