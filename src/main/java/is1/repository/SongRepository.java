package is1.repository;

import is1.model.Song;

import java.util.List;

public interface SongRepository extends BaseRepository<Song, Long> {
    List<Song> getTop(int firstResult, int maxResults);
    List<Song> getSearchResults(String query);
}
