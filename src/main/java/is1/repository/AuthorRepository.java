package is1.repository;

import is1.model.Author;

public interface AuthorRepository extends BaseRepository<Author, Long> {
}
