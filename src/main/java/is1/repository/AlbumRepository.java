package is1.repository;

import is1.model.Album;

public interface AlbumRepository extends BaseRepository<Album, Long> {
}
