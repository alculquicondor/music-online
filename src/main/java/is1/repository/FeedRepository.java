package is1.repository;

import is1.model.Feed;

public interface FeedRepository extends BaseRepository<Feed, Long> {
}
