package is1.repository;

import is1.model.Account;
import is1.model.Like;
import is1.model.Song;

public interface LikeRepository extends BaseRepository<Like, Long> {
    boolean likes(Account account, Song song);
}
