package is1.repository;

import java.util.List;

public interface BaseRepository<E, K> {
    E save(E e);
    Boolean remove(E entity);
    Boolean removeById(K id);
    E findById(K id);
    List<E> findEntries(int firstResult, int maxResults);
    E merge(E entity);
    long count();
}
