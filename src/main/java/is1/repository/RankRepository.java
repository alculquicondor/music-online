package is1.repository;

import is1.model.Rank;
import is1.repository.BaseRepository;

public interface RankRepository extends BaseRepository<Rank, Long> {
}
