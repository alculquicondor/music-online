package is1.repository.jpa;

import is1.model.Account;
import is1.model.FeedEntry;
import is1.repository.FeedEntryRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.PersistenceException;
import java.util.List;

@Transactional
@Repository
public class JpaFeedEntryRepository extends JpaBaseRepository<FeedEntry, Long> implements FeedEntryRepository {

    @Override
    public List<FeedEntry> getLatestEntriesForAccount(Account account, int firstResult, int maxResults) {
        String jpql = "SELECT fe FROM Account ac JOIN ac.following f JOIN f.entries fe " +
                "WHERE ac = :account ORDER BY fe.timestamp DESC";
        try {
            return entityManager
                    .createQuery(jpql, FeedEntry.class)
                    .setParameter("account", account)
                    .setFirstResult(firstResult)
                    .setMaxResults(maxResults)
                    .getResultList();
        } catch (PersistenceException e) {
            return null;
        }
    }
}
