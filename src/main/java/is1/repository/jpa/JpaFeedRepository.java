package is1.repository.jpa;


import is1.model.Feed;
import is1.repository.FeedRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public class JpaFeedRepository extends JpaBaseRepository<Feed,Long> implements FeedRepository{
}
