package is1.repository.jpa;

import is1.model.PlayList;
import is1.repository.PlayListRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public class JpaPlayListRepository extends JpaBaseRepository<PlayList,Long> implements PlayListRepository {
}
