package is1.repository.jpa;


import is1.model.Album;
import is1.repository.AlbumRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public class JpaAlbumRepository extends JpaBaseRepository<Album,Long> implements AlbumRepository {

}