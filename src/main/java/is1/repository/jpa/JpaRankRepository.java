package is1.repository.jpa;

import is1.model.Rank;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public class JpaRankRepository extends JpaBaseRepository<Rank,Long> {
}
