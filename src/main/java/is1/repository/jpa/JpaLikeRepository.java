package is1.repository.jpa;

import is1.model.Account;
import is1.model.Like;
import is1.model.Song;
import is1.repository.LikeRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.PersistenceException;

@Repository
@Transactional(readOnly = true)
public class JpaLikeRepository extends JpaBaseRepository<Like,Long> implements LikeRepository {

    @Override
    public boolean likes(Account account, Song song) {
        String jpql = "SELECT l FROM Like l WHERE l.account = :account AND " +
                "l.song = :song";
        try {
            return entityManager
                    .createQuery(jpql, Like.class)
                    .setParameter("account", account)
                    .setParameter("song", song)
                    .getSingleResult() != null;
        } catch (PersistenceException e) {
            return false;
        }
    }
}
