package is1.repository.jpa;

import is1.model.Log;
import is1.repository.LogRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public class JpaLogRepository extends JpaBaseRepository<Log,Long> implements LogRepository{
}
