package is1.repository.jpa;

import is1.model.Author;
import is1.repository.AuthorRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public class JpaAuthorRepository extends JpaBaseRepository<Author,Long> implements AuthorRepository {
}
