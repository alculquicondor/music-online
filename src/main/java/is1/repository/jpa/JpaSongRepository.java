package is1.repository.jpa;

import is1.model.Song;


import is1.repository.SongRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.PersistenceException;
import java.util.List;

@Repository
@Transactional(readOnly = true)
public class JpaSongRepository extends JpaBaseRepository<Song, Long> implements SongRepository {

    public List<Song> getTop(int firstResult, int maxResults) {
        String jpql = "SELECT s FROM Like v JOIN v.song s " +
                " GROUP BY s.id ORDER BY count(s.id) DESC";
        try {
            return entityManager
                    .createQuery(jpql, Song.class)
                    .setFirstResult(firstResult)
                    .setMaxResults(maxResults)
                    .getResultList();
        } catch (PersistenceException e) {
            return null;
        }
    }

    public List<Song> getSearchResults(String query) {
        // TODO : Add search by Authors
        String jpql = "SELECT s FROM Song s WHERE lower(s.title) LIKE :like";
        try {
            return entityManager
                    .createQuery(jpql, Song.class)
                    .setParameter("like", "%" + query.toLowerCase() + "%")
                    .setFirstResult(0)
                    .setMaxResults(50)
                    .getResultList();
        } catch (PersistenceException e) {
            return null;
        }
    }

}
