package is1.repository.jpa;

import is1.model.BaseEntity;
import is1.repository.BaseRepository;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public abstract class JpaBaseRepository<E extends BaseEntity<K>, K> implements BaseRepository<E, K> {

    @PersistenceContext
    protected EntityManager entityManager;

    protected Class<E> entityClass = getEntityClass();

    protected Class<K> primaryKeyClass = getPrimaryKeyClass();

    @Override
    @Transactional
    public E save(E entity) {
        entityManager.persist(entity);
        return entity;
    }

    @Override
    @Transactional
    public Boolean remove(E entity) {
        if (entity != null) {
            if (this.entityManager.contains(entity)) {
                this.entityManager.remove(entity);
            } else {
                E attached = findById(entity.getId());
                this.entityManager.remove(attached);
            }
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    @Override
    @Transactional
    public Boolean removeById(K id) {
        E e = findById(id);
        return remove(e);
    }

    @Override
    @Transactional
    public E findById(K id) {
        if (id == null)
            return null;
        return entityManager.find(entityClass, id);
    }

    @Override
    public List<E> findEntries(int firstResult, int maxResults) {
        String jpql = "SELECT o FROM " + entityClass.getSimpleName() + " o";
        return entityManager.createQuery(jpql, entityClass).setFirstResult(firstResult).
                setMaxResults(maxResults).getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public E merge(E entity) {
        return entityManager.merge(entity);
    }

    @Override
    public long count() {
        return entityManager.createQuery(
                "SELECT count(o) FROM " + entityClass.getSimpleName() + " o",
                Long.class).getSingleResult();
    }

    @SuppressWarnings("unchecked")
    public Class<E> getEntityClass() {
        if (entityClass == null) {
            ParameterizedType thisType = (ParameterizedType)getClass().getGenericSuperclass();
            entityClass = (Class<E>) thisType.getActualTypeArguments()[0];
        }
        return entityClass;
    }

    @SuppressWarnings("unchecked")
    public Class<K> getPrimaryKeyClass() {
        if (primaryKeyClass == null) {
            ParameterizedType thisType = (ParameterizedType)getClass().getGenericSuperclass();
            primaryKeyClass = (Class<K>) thisType.getActualTypeArguments()[1];
        }
        return primaryKeyClass;
    }

}
