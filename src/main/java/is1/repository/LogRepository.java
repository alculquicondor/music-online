package is1.repository;

import is1.model.Log;

public interface LogRepository extends BaseRepository<Log, Long> {
}
