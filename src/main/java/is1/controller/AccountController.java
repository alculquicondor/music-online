package is1.controller;

import is1.model.Account;
import is1.repository.AccountRepository;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@Secured("ROLE_USER")
class AccountController {

	@Autowired
	private AccountRepository accountRepository;

	@RequestMapping(value = "account/current", method = RequestMethod.GET)
	public String account(Principal principal) {
		return "redirect:/" + principal.getName() + "/playlists";
	}

	@RequestMapping(value = "{username}", method = RequestMethod.GET)
	public String account(@PathVariable String username) {
		return "redirect:/" + username + "/playlists";
	}

	@RequestMapping(value = "{username}/follow", method = RequestMethod.POST)
	public String follow(@PathVariable String username, Principal principal) {
		Account account = accountRepository.findByUsername(principal.getName());
		Account accountToFollow = accountRepository.findByUsername(username);
		if (accountToFollow != null) {
			account.addFollowing(accountToFollow.getFeed());
			accountRepository.merge(account);
		}
		return "redirect:/" + username + "/playlists";
	}
}
