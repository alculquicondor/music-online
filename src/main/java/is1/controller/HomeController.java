package is1.controller;

import java.security.Principal;
import java.util.List;

import is1.model.Account;
import is1.model.Song;
import is1.service.AccountService;
import is1.service.SongService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {

	@Autowired
	private AccountService accountService;

	@Autowired
	private SongService songService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(ModelMap model, Principal principal) {
		model.addAttribute("authenticated", principal != null);
		List<Song> songs = songService.getTopSongs(0, 100);
		if (songs.size() > 0)
            model.addAttribute("youtubeId", songs.get(0).getYoutubeId());
		model.addAttribute("songs", songs);

		if (principal != null) {
			Account account = accountService.getAccountByUsername(principal.getName());
			model.addAttribute("notifications", accountService.getNotifications(account, 0, 20));
			model.addAttribute("account", account);
			return "home/homeSignedIn";
		}
		return "home/homeNotSignedIn";
	}
}
