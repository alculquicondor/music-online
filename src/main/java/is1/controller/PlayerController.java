package is1.controller;

import is1.model.Account;
import is1.model.Album;
import is1.model.Author;
import is1.model.Song;
import is1.service.AccountService;
import is1.service.AlbumService;
import is1.service.AuthorService;
import is1.service.SongService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@Controller
@RequestMapping("")
public class PlayerController {

    @Autowired
    private AuthorService authorService;

    @Autowired
    private AlbumService albumService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private SongService songService;

    @RequestMapping(value = "search")
    public String search(@RequestParam("q") String query, Model model, Principal principal) {
        model.addAttribute("authenticated", principal != null);
        model.addAttribute("q", query);
        model.addAttribute("songs", songService.searchSongs( query ));
        return "player/search";
    }

    @RequestMapping("author/{id}")
    public String showAuthor(@PathVariable long id, Model model, Principal principal) {
        Author author = authorService.getAuthor(id);
        if (principal != null) {
            Account account = accountService.getAccountByUsername(principal.getName());
            model.addAttribute("authenticated", true);
            model.addAttribute("followBtn", !account.getFollowing().contains(author.getFeed()));
        }
        model.addAttribute("author", author);
        return "player/author";
    }

    @RequestMapping(value = "author/{id}/follow", method = RequestMethod.POST)
    public String followAuthor(@PathVariable long id, Principal principal) {
        if (principal != null) {
            Account account = accountService.getAccountByUsername(principal.getName());
            Author author = authorService.getAuthor(id);
            if (!account.getFollowing().contains(author.getFeed())) {
                account.addFollowing(author.getFeed());
                accountService.updateAccount(account);
            }
        }
        return "redirect:/author/" + Long.toString(id);
    }

    @RequestMapping("album/{id}")
    public String showAlbum(@PathVariable long id, Model model, Principal principal) {
        if (principal != null) {
            model.addAttribute("authenticated", true);
            model.addAttribute("account", accountService.getAccountByUsername(principal.getName()));
        } else {
            model.addAttribute("authenticated", false);
        }
        Album album = albumService.getAlbum(id);
        model.addAttribute("album", album);
        if (album.getSongs().size() > 0)
            model.addAttribute("youtubeId", album.getSongs().get(0).getYoutubeId());
        model.addAttribute("songs", album.getSongs());
        return "player/album";
    }

    @RequestMapping(value = "like/{id}", method = RequestMethod.POST)
    @Secured("ROLE_USER")
    @ResponseBody
    public String likeSong(@PathVariable long id, Principal principal) {
        Account account = accountService.getAccountByUsername(principal.getName());
        Song song = songService.getSong(id);
        songService.likeSong(account, song);
        return "something";
    }
}
