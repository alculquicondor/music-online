package is1.controller;

import is1.bean.PlayListBean;
import is1.model.Account;
import is1.model.Feed;
import is1.model.PlayList;
import is1.model.Song;
import is1.service.AccountService;
import is1.service.PlayListService;
import is1.service.SongService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

@Controller
@RequestMapping(value = "{username}/playlists")
@Secured("ROLE_USER")
public class PlayListController {

    @Autowired
    private PlayListService playListService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private SongService songService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String listPlayLists(Model model, @PathVariable String username, Principal principal) {
        Account account = accountService.getAccountByUsername(username);
        model.addAttribute("account", account);
        if (principal != null) {
            Account loggedAccount = accountService.getAccountByUsername(principal.getName());
            boolean followBtn = !username.equals(principal.getName()) &&
                    !loggedAccount.getFollowing().contains(account.getFeed());
            model.addAttribute("authenticated", true);
            model.addAttribute("followBtn", followBtn);
            if (principal.getName().equals(username))
                model.addAttribute(new PlayListBean());
        }
        return "user/playlists";
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public String newPlayList(@Valid @ModelAttribute PlayListBean playListBean, Errors errors,
                             ModelMap model, @PathVariable String username, Principal principal) {
        if (principal.getName().equals(username)) {
            if (!errors.hasErrors()) {
                Account account = accountService.getAccountByUsername(principal.getName());
                playListService.createPlayList(playListBean.createPlayList(account));
                return "redirect:/" + username + "/playlists";
            }
            model.addAttribute("errors", errors);
        }
        return "user/playlists";
    }

    @RequestMapping(value = "{id}", method =  RequestMethod.GET)
    public String showPlayList(Model model, @PathVariable long id, @PathVariable String username,
                               Principal principal) {
        PlayList playList = playListService.getPlayList(id);
        if (principal != null) {
            Account loggedAccount = accountService.getAccountByUsername(principal.getName());
            boolean followBtn = !username.equals(principal.getName()) &&
                    !loggedAccount.getFollowing().contains(playList.getFeed());
            model.addAttribute("authenticated", true);
            model.addAttribute("followBtn", followBtn);
        }
        model.addAttribute("username", username);
        model.addAttribute("playList", playList);
        model.addAttribute("songs", playList.getSongs());
        return "user/playlist";
    }

    @RequestMapping(value = "{id}/add/{sid}", method =  RequestMethod.POST)
    @ResponseBody
    public String addSong(@PathVariable String username,
                          @PathVariable long id, @PathVariable long sid, Principal principal) {
        PlayList playList = playListService.getPlayList(id);
        if (username.equals(principal.getName())) {
            Song song = songService.getSong(sid);
            playList.addSong(song);
            playListService.updatePlayList(playList);
            playListService.addFeedEntry(playList, "Song \"" + song.getTitle() + "\"added");
        }
        return "something";
    }

    @RequestMapping(value = "{id}/follow", method =  RequestMethod.POST)
    public String follow(@PathVariable String username, @PathVariable long id, Principal principal) {
        Account account = accountService.getAccountByUsername(principal.getName());
        PlayList playList = playListService.getPlayList(id);
        if (playList != null) {
            Feed feed = playList.getFeed();
            if (!account.getFollowing().contains(feed)) {
                account.addFollowing(feed);
                accountService.updateAccount(account);
            }
            return "redirect:/" + username + "/playlists/" + Long.toString(playList.getId());
        }
        return "redirect:/generalError";
    }
}
