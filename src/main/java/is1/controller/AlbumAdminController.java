package is1.controller;

import is1.bean.AlbumBean;
import is1.bean.SongBean;
import is1.model.Album;
import is1.model.Author;
import is1.model.Song;
import is1.service.AlbumService;
import is1.service.AuthorService;
import is1.service.SongService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
@RequestMapping(value = "admin/albums")
@Secured("ROLE_ADMIN")
public class AlbumAdminController {

    @Autowired
    private AuthorService authorService;

    @Autowired
    private AlbumService albumService;

    @Autowired
    private SongService songService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String listAlbums(Model model) {
        model.addAttribute("authenticated", true);
        model.addAttribute("albums", albumService.getAlbums(0, 10));
        model.addAttribute("authors", authorService.getAuthors(0, 10));
        model.addAttribute("albumBean",new AlbumBean());
        return "admin/albums";
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public String newAlbums(@Valid @ModelAttribute AlbumBean albumBean,
                             Errors errors, ModelMap model) {
        if (!errors.hasErrors()) {
            Album album = albumBean.createAlbum();
            Author author = authorService.getAuthor(albumBean.getAuthor());
            album.addAuthor(author);
            albumService.createAlbum( album );
            authorService.addFeedEntry(author, "Album \"" + album.getTitle() + "\"released");
            return "redirect:/admin/albums";
        }
        model.addAttribute("errors", errors);
        return "admin/albums";
    }

    @RequestMapping(value = "{id}", method =  RequestMethod.GET)
    public String showSongs(Model model, @PathVariable long id) {
        model.addAttribute("authenticated", true);
        Album album = albumService.getAlbum(id);
        model.addAttribute("album", album );
        model.addAttribute("songs", album.getSongs() );
        model.addAttribute("submitPath","/admin/albums/"+Long.toString(id));
        model.addAttribute("songBean",new SongBean());
        return "admin/album";
    }

    @RequestMapping(value = "{id}", method = RequestMethod.POST)
    public String newSong(@Valid @ModelAttribute SongBean songBean,
                            Errors errors, ModelMap model , @PathVariable long id) {
        if (!errors.hasErrors()) {
            Album album = albumService.getAlbum(id);
            Song song = new Song( songBean.getTitle() , songBean.getYoutubeId() , album.getAuthors() , album );
            songService.createSong(song);
            return "redirect:/admin/albums/"+Long.toString(id);
        }
        model.addAttribute("errors", errors);
        return "admin/albums";
    }

}