package is1.controller;

import is1.bean.AuthorBean;
import is1.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
@RequestMapping(value = "admin/authors")
@Secured("ROLE_ADMIN")
public class AuthorAdminController {

    @Autowired
    private AuthorService authorService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String listAuthors(Model model) {
        model.addAttribute("authenticated", true);
        model.addAttribute("authors", authorService.getAuthors(0, 10));
        model.addAttribute(new AuthorBean());
        return "admin/authors";
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public String newAuthors(@Valid @ModelAttribute AuthorBean authorBean,
                             Errors errors, ModelMap model) {
        if (!errors.hasErrors()) {
            authorService.createAuthor(authorBean.createAuthor());
            return "redirect:/admin/authors";
        }
        model.addAttribute("errors", errors);
        return "admin/authors";
    }
}
