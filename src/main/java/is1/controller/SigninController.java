package is1.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SigninController {

	@RequestMapping(value = "signin")
	public String signIn(@RequestParam(value="error", required=false) boolean error, ModelMap model) {
		model.addAttribute("error", error);
		return "signin/signin";
	}
}
