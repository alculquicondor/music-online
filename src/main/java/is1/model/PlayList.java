package is1.model;

import javax.persistence.*;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;

@SuppressWarnings("serial")
@Entity
@Table(name = "playlist")
public class PlayList implements PlayListable, BaseEntity<Long>, Followable {

    @Id
    @SequenceGenerator(name = "ALBUM_ID_GENERATOR", sequenceName = "ALBUM_ID_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ALBUM_ID_GENERATOR")
    private Long id;


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "feed_id", nullable = false)
    private Feed feed;

    @Column(nullable = false)
    private String title;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "playlist_song", joinColumns = {
            @JoinColumn(name = "playlist_id", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "song_id",
                    nullable = false, updatable = false) })
    private List<Song> songs;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner_id", nullable = false)
    private Account owner;

    @Column(nullable = false)
    private Timestamp createdOn;

    protected PlayList() {
    }

    public PlayList(String title, List<Song> songs, Account owner) {
        this.title = title;
        this.songs = songs;
        this.owner = owner;
    }

    public PlayList(String title, List<Song> songs, Account owner, Timestamp createdOn, Feed feed) {
        this.title = title;
        this.songs = songs;
        this.owner = owner;
        this.feed = feed;
        this.createdOn = createdOn;
    }

    public String getTitle() {
        return title;
    }

    public List<Song> getSongs() {
        return songs;
    }

    public void addSong(Song song) {
        songs.add( song );
    }

    public Account getOwner() {
        return owner;
    }

    public Feed getFeed() {
        return feed;
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Long getId() {
        return id;
    }

    public String getType() {
        return "Playlist";
    }

    public String getName() {
        return title;
    }

    public String getLink() {
        return owner.getUsername() + "/playlists/" + Long.toString(getId());
    }
}
