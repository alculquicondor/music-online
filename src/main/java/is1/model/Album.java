package is1.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.FetchType;
import javax.persistence.CascadeType;
import java.io.Serializable;
import java.sql.Date;
import java.util.List;

@SuppressWarnings("serial")
@Entity
@Table(name = "album")
public class Album implements Serializable, PlayListable, BaseEntity<Long> {

    @Id
    @SequenceGenerator(name = "ALBUM_ID_GENERATOR", sequenceName = "ALBUM_ID_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ALBUM_ID_GENERATOR")
    private Long id;

    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private Date pubDate;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "album_author", joinColumns = {
            @JoinColumn(name = "album_id", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "author_id",
                    nullable = false, updatable = false) })
    private List<Author> authors;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "album")
    public List<Song> songs;

    protected Album() {

    }

    public Album(String title, Date pubDate, List<Author> authors) {
        this.title = title;
        this.pubDate = pubDate;
        this.authors = authors;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getPubDate() {
        return pubDate;
    }

    public void setPubDate(Date pubDate) {
        this.pubDate = pubDate;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public void addAuthor(Author author){ this.authors.add(author); }

    public List<Song> getSongs() {
        return songs;
    }

    public void setSongs(List<Song> songs) {
        this.songs = songs;
    }
}
