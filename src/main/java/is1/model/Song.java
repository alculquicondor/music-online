package is1.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.FetchType;
import javax.persistence.CascadeType;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
@Entity
@Table(name = "song")
public class Song implements Serializable, BaseEntity<Long> {

    @Id
    @SequenceGenerator(name = "SONG_ID_GENERATOR", sequenceName = "SONG_ID_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SONG_ID_GENERATOR")
    private Long id;

    @Column(nullable = false)
    private String title;

    @Column(nullable = false, length = 11)
    private String youtubeId;

    @Column(columnDefinition = "TEXT")
    private String lyrics;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "song_author", joinColumns = {
            @JoinColumn(name = "song_id", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "author_id",
                    nullable = false, updatable = false) })
    private List<Author> authors;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "album_id", nullable = false)
    private Album album;

    @Column
    private double rank = 0;

    protected Song() {

    }

    public Song(String title, String youtubeId, List<Author> authors, Album album) {
        this.title = title;
        this.youtubeId = youtubeId;
        this.authors = authors;
        this.album = album;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYoutubeId() {
        return youtubeId;
    }

    public void setYoutubeId(String youtubeId) {
        this.youtubeId = youtubeId;
    }

    public String getLyrics() {
        return lyrics;
    }

    public void setLyrics(String lyrics) {
        this.lyrics = lyrics;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }

    public double getRank() {
        return rank;
    }

    public void setRank(double rank) {
        this.rank = rank;
    }
}
