package is1.model;

/**
 * Created by eileen on 10/11/14.
 */

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.OneToOne;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import java.io.Serializable;
import java.sql.Timestamp;

@SuppressWarnings("serial")
@Entity
@Table(name = "liket")
public class Like implements Serializable, BaseEntity<Long> {

    @Id
    @SequenceGenerator(name = "LINK_ID_GENERATOR", sequenceName = "LINK_ID_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LINK_ID_GENERATOR")
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id", nullable = false)
    private Account account;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "song_id", nullable = false)
    private Song song;

    @Override
    public Long getId() {
        return id;
    }

    protected Like() {
        
    }

    public Like(Account account, Song song) {
        this.account = account;
        this.song = song;
    }

    public void setSong(Song song) {
        this.song = song;
    }

    public Song getSong() {
        return this.song;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Account getAccount(){
        return this.account;
    }

}
